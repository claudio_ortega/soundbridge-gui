#!/usr/bin/env bash

echo "BITBUCKET_REPO_OWNER=["${BITBUCKET_REPO_OWNER}"]"
echo "BITBUCKET_REPO_SLUG=["${BITBUCKET_REPO_SLUG}"]"

for next_file in ./build/distributions/*.zip
do
    echo "attempting to upload file:"${next_file}

    curl_command="curl -X POST \
        --user ${BITBUCKET_REPO_OWNER}:${APP_PASSWORD} \
        https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads \
        --form files=@${next_file}"

    echo "will execute upload command: ["${curl_command}"]"

    ${curl_command}

    if [[ ($? -eq 0) ]]; then
        echo "upload succeeded for file:"${next_file}
    else
        echo "upload failed for file:"${next_file}
        exit -1
    fi
done