#!/usr/bin/env bash

ci/build_application.bash ${*}

if [[ ($# -eq 0) ]]; then
    echo "detected a local build, skip uploading artifacts"
else
    echo "detected a ci server build, proceeding to upload artifacts"
    ci/upload_installers.bash
    echo "artifacts uploaded"
fi
