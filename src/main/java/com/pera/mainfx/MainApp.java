/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 */

package com.pera.mainfx;

import com.google.common.base.Preconditions;
import com.jfoenix.controls.*;
import com.pera.cmdline.*;
import com.pera.javafx.*;
import com.pera.util.LogConfigurator;
import com.pera.util.RestUtil;
import com.pera.util.SystemUtil;
import javafx.application.*;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.*;
import javafx.stage.*;
import org.apache.commons.lang3.time.*;
import org.apache.logging.log4j.*;
import org.json.JSONObject;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import java.util.function.*;

public class MainApp extends Application {

    volatile private static StartupConfigInfo startupConfig;
    volatile private static Logger logger;

    private final Properties props;
    private final ThreadPoolExecutor executor;

    private String restServerOS;
    private String restServerVS;
    private int restPortOS;
    private int restPortVS;

    // used from shells
    public static void main( String[] args )
    {
        System.exit( staticMain( false, args ) );
    }

    // used from tests
    public static int mainAlt( String ... args  )
    {
        return staticMain( true, args );
    }

    private static int staticMain(boolean executingFromTestCase, String ... args  ) {
        try {

            LogConfigurator.singleton().init();
            logger = LogManager.getLogger();

            final com.pera.cmdline.CommandLineParser commandLineParser = CommandLineParser.create();

            try {
                startupConfig = commandLineParser.getConfigFromArgs( args, StartupConfigInfo.DEFAULT );
            }
            catch ( Exception e ) {
                System.out.println( e.getMessage() );
                return 1;
            }

            if ( startupConfig.getHelpRequested() ) {
                commandLineParser.printHelp( System.out );
                System.out.println( "\nversion " + SystemUtil.getGitVersion ( true ) + "\n" );
                return 0;
            }

            if ( startupConfig.isVersionRequested() ) {
                System.out.println( "\nversion " + SystemUtil.getGitVersion ( true ) + "\n" );
                return 0;
            }

            // our test code configures log4j before executing the mainAlt(),
            // so then, we should skip the log configuration whenever coming from test code
            if ( ! executingFromTestCase ) {
                LogConfigurator.singleton().setLevel( startupConfig.getLogLevel() ) ;
            }

            launch(args);

            return 0;
        }

        catch ( Exception e ) {
            logger.error( e.getMessage(), e );
            return 1;
        }
    }

    // javafx needs the constructor to be public, go figure
    public MainApp()
    {
        props = new Properties();
        executor = new ThreadPoolExecutor(
            10,
            10,
            0,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>( ) );
    }

    @Override
    public void start ( Stage primaryStage ) throws Exception {

        iLogHeader( logger );

        final File configFile = new File( startupConfig.getConfigPath() );

        Preconditions.checkState(
            configFile.exists() && configFile.canRead(),
            String.format( "failing in reading config from file: %s", configFile )
        );

        logger.info( "read config from file: {}", configFile );

        try (final InputStream inStream = new FileInputStream( configFile ) ) {
            props.load(inStream);
        }

        final int TEXT_AREA_WIDTH = 800;
        final int TEXT_AREA_HEIGHT = 900;

        final TextArea osPopupTextArea = new TextArea();
        osPopupTextArea.setEditable( false );
        osPopupTextArea.setPrefSize(TEXT_AREA_WIDTH, TEXT_AREA_HEIGHT);
        osPopupTextArea.setFont ( Font.font ( "Monospaced", FontWeight.BOLD, 13 ) );

        final TextArea vsPopupTextArea = new TextArea();
        vsPopupTextArea.setEditable( false );
        vsPopupTextArea.setPrefSize(TEXT_AREA_WIDTH, TEXT_AREA_HEIGHT);
        vsPopupTextArea.setFont ( Font.font ( "Monospaced", FontWeight.BOLD, 13 ) );

        final AtomicBoolean osTextAreaIsOpen = new AtomicBoolean(false);
        final StatusIndicator osStatusIndicator = StatusIndicator.create( () ->
        {
            osTextAreaIsOpen.set( true );
            iCreatePopupWindow(
                primaryStage,
                osPopupTextArea,
                ev -> osTextAreaIsOpen.set( false ) );
        } );

        final AtomicBoolean vsTextAreaIsOpen = new AtomicBoolean(false);
        final StatusIndicator vsStatusIndicator = StatusIndicator.create( () ->
        {
            vsTextAreaIsOpen.set( true );
            iCreatePopupWindow(
                primaryStage,
                vsPopupTextArea,
                ev -> vsTextAreaIsOpen.set( false ) );
        } );

        final Label osAppTimeLbl = new Label();
        final Label vsAppTimeLbl = new Label();

        restServerOS = iGetOSRestServerFromConfig("rest_server_os");
        restServerVS = iGetOSRestServerFromConfig("rest_server_vs");
        Preconditions.checkState( !restServerOS.isEmpty(), "restServerOS is empty" );
        Preconditions.checkState( !restServerVS.isEmpty(), "restServerVS is empty" );

        restPortOS = Integer.parseInt( (String) props.get("rest_port_os" ) );
        restPortVS = Integer.parseInt( (String) props.get("rest_port_vs" ) );

        logger.info("looking for an OS at: {}:{}", restServerOS, restPortOS );
        logger.info("looking for a VS at: {}:{}", restServerVS, restPortVS );

        // ptt
        final Button pttOnButton = new JFXButton();
        pttOnButton.setPrefSize( 120, 120 );
        pttOnButton.setText( "PTT" );
        pttOnButton.getStyleClass().add( "ptt-button" );

        // vhf selection
        final int bcmIndexVhf1 = Integer.parseInt( (String) props.get("bcm_index_vhf1") );
        final int bcmIndexVhf2 = Integer.parseInt( (String) props.get("bcm_index_vhf2") );
        final JFXCheckBox vhf1 = new JFXCheckBox( "Radio principal" );
        final JFXCheckBox vhf2 = new JFXCheckBox("Handy");
        vhf1.setSelected(true);
        vhf2.setSelected(false);

        vhf1.selectedProperty().addListener(
            (obs, oldValue, NewValue) -> executor.submit( () -> iAddOrRemoveBcm( bcmIndexVhf1, vhf1.isSelected() ) ) );
        vhf2.selectedProperty().addListener(
            (obs, oldValue, NewValue) -> executor.submit( () -> iAddOrRemoveBcm( bcmIndexVhf2, vhf2.isSelected() ) ) );

        // auxiliary
        final int bcmIndexAuxiliary1 = Integer.parseInt((String) props.get("bcm_index_auxiliary_1"));
        final JFXToggleButton auxCheckBox1 = new JFXToggleButton();
        auxCheckBox1.setText("Luz de Plataforma");
        auxCheckBox1.selectedProperty().addListener(
            (obs, oldValue, NewValue) -> executor.submit(() -> iSetGPIOPinState(bcmIndexAuxiliary1, auxCheckBox1.isSelected())));
        iSetGPIOPinState(bcmIndexAuxiliary1, false);

        final int bcmIndexAuxiliary2 = Integer.parseInt((String) props.get("bcm_index_auxiliary_2"));
        final JFXToggleButton auxCheckBox2 = new JFXToggleButton();
        auxCheckBox2.setText("Luz de Pista");

        auxCheckBox2.selectedProperty().addListener(
            (obs, oldValue, NewValue) -> executor.submit(() -> iSetGPIOPinState(bcmIndexAuxiliary2, auxCheckBox2.isSelected())));
        iSetGPIOPinState(bcmIndexAuxiliary2, false);

        final int bcmIndexAuxiliary3 = Integer.parseInt((String) props.get("bcm_index_auxiliary_3"));
        final JFXToggleButton auxCheckBox3 = new JFXToggleButton();
        auxCheckBox3.setText("Luz de Rodaje");

        auxCheckBox3.selectedProperty().addListener(
            (obs, oldValue, NewValue) -> executor.submit(() -> iSetGPIOPinState(bcmIndexAuxiliary3, auxCheckBox3.isSelected())));
        iSetGPIOPinState(bcmIndexAuxiliary3, false);

        final int bcmIndexAuxiliary4 = Integer.parseInt((String) props.get("bcm_index_auxiliary_4"));
        final JFXToggleButton auxCheckBox4 = new JFXToggleButton();
        auxCheckBox4.setText("Alarma Local");

        auxCheckBox4.selectedProperty().addListener(
            (obs, oldValue, NewValue) -> executor.submit(() -> iSetGPIOPinState(bcmIndexAuxiliary4, auxCheckBox4.isSelected())));
        iSetGPIOPinState(bcmIndexAuxiliary4, false);

        // panel construction
        //
        final AtomicInteger row = new AtomicInteger(0);

        // GridPane
        final GridPane gridPane = new GridPane();
        gridPane.setGridLinesVisible(false);
        gridPane.setPadding(new Insets(10,10,10,10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);

        final boolean experimentAl = false;

        if ( experimentAl )
        {
            final JFXComboBox<String> vsSelectionCombo = new JFXComboBox<>();
            vsSelectionCombo.getItems().addAll( "Ezeiza - International", "Mar del Plata" );
            vsSelectionCombo.getSelectionModel ( ).select ( 0 );
            vsSelectionCombo.setFocusColor( Color.TRANSPARENT );
            vsSelectionCombo.setUnFocusColor( Color.TRANSPARENT );
            iAddStrut( gridPane, row.incrementAndGet(), 10 );

            vsSelectionCombo.getStyleClass().add( "vs-combo" );
            gridPane.add(vsSelectionCombo, 0, row.incrementAndGet());
            GridPane.setHalignment( vsSelectionCombo, HPos.CENTER);
            GridPane.setFillWidth( vsSelectionCombo, true );
            GridPane.setColumnSpan(vsSelectionCombo, 2);

            iAddStrut( gridPane, row.incrementAndGet(), 10 );
            final Separator separator = new Separator();
            gridPane.add(separator, 0, row.incrementAndGet());
            GridPane.setColumnSpan(separator, 2);
        }

        iAddStrut( gridPane, row.incrementAndGet(), 20 );

        gridPane.add(pttOnButton, 0, row.incrementAndGet());
        GridPane.setHalignment(pttOnButton, HPos.CENTER);
        GridPane.setColumnSpan(pttOnButton, 2);

        iAddStrut( gridPane, row.incrementAndGet(), 20 );

        gridPane.add(vhf1, 0, row.incrementAndGet());
        GridPane.setHalignment(vhf1, HPos.CENTER);

        gridPane.add(vhf2, 1, row.get());
        GridPane.setHalignment(vhf2, HPos.CENTER);

        iAddStrut( gridPane, row.incrementAndGet(), 15 );
        final Separator separatorA = new Separator();
        gridPane.add(separatorA, 0, row.incrementAndGet());
        GridPane.setColumnSpan(separatorA, 2);
        gridPane.add(auxCheckBox1, 0, row.incrementAndGet());
        gridPane.add(auxCheckBox2, 1, row.get());
        gridPane.add(auxCheckBox3, 0, row.incrementAndGet());
        gridPane.add(auxCheckBox4, 1, row.get());

        // OS
        iAddStrut( gridPane, row.incrementAndGet(), 1 );
        final Separator separatorB = new Separator();
        gridPane.add(separatorB, 0, row.incrementAndGet());
        iAddStrut( gridPane, row.incrementAndGet(), 10 );
        GridPane.setColumnSpan(separatorB, 2);

        gridPane.add(new Label("Estado de torre virtual"), 0, row.incrementAndGet());
        gridPane.add(osStatusIndicator.getLabel(), 1, row.get());
        osStatusIndicator.getLabel().setPrefHeight(24);
        GridPane.setHalignment(osStatusIndicator.getLabel(), HPos.RIGHT);
        GridPane.setValignment(osStatusIndicator.getLabel(), VPos.CENTER);

        iAddStrut( gridPane, row.incrementAndGet(), 1 );

        gridPane.add(new Label("Tiempo en servicio:"), 0, row.incrementAndGet());
        gridPane.add(osAppTimeLbl, 1, row.get());
        GridPane.setHalignment(osAppTimeLbl, HPos.RIGHT);
        GridPane.setValignment(osAppTimeLbl, VPos.CENTER);

        // VS
        iAddStrut( gridPane, row.incrementAndGet(), 10 );
        final Separator separatorC = new Separator();
        gridPane.add(separatorC, 0, row.incrementAndGet());
        GridPane.setColumnSpan(separatorC, 2);

        iAddStrut( gridPane, row.incrementAndGet(), 1 );

        gridPane.add(new Label("Estado de torre real"), 0, row.incrementAndGet());
        gridPane.add(vsStatusIndicator.getLabel(), 1, row.get());
        vsStatusIndicator.getLabel().setPrefHeight(24);
        GridPane.setHalignment(vsStatusIndicator.getLabel(), HPos.RIGHT);
        GridPane.setValignment(vsStatusIndicator.getLabel(), VPos.CENTER);

        iAddStrut( gridPane, row.incrementAndGet(), 1 );

        gridPane.add(new Label("Tiempo en servicio:"), 0, row.incrementAndGet());
        gridPane.add(vsAppTimeLbl, 1, row.get());
        GridPane.setHalignment(vsAppTimeLbl, HPos.RIGHT);
        GridPane.setValignment(vsAppTimeLbl, VPos.CENTER);

        iAddStrut( gridPane, row.incrementAndGet(), 1 );

        // Scene
        final Scene scene = new Scene(gridPane);
        final KeystrokeHandler keystrokeHandler = new KeystrokeHandler(scene);
        scene.getStylesheets().clear();
        scene.getStylesheets().add("com/pera/mainfx/1.css");
        primaryStage.setScene(scene);
        primaryStage.setTitle("RoIP");
        primaryStage.setResizable(false);
        primaryStage.show();

        pttOnButton.setOnMouseReleased(
            ev -> executor.submit(
                () ->
                {
                    if ( ! keystrokeHandler.isActivated() ) {
                        iSendPtt(false, restServerOS, restPortOS);
                    }
                }
            )
        );

        pttOnButton.setOnMousePressed(
            ev -> executor.submit(
                () ->
                {
                    if ( ! keystrokeHandler.isActivated() ) {
                        iSendPtt(true, restServerOS, restPortOS);
                    }
                    else {
                        iSendShutdownToVS( restServerVS, restPortVS );
                    }
                }
            )
        );

        new ScheduledThreadPoolExecutor( 1 ).scheduleAtFixedRate (
            ( ) -> {
                iUpdateStatus(
                    true,
                    restServerOS,
                    restPortOS,
                    osStatusIndicator,
                    osAppTimeLbl,
                    string -> {
                        if ( ! osTextAreaIsOpen.get() ) {
                            osPopupTextArea.setText(string);
                        }
                    }
                );

                iUpdateStatus(
                    false,
                    restServerVS,
                    restPortVS,
                    vsStatusIndicator,
                    vsAppTimeLbl,
                    string -> {
                        if ( ! vsTextAreaIsOpen.get() ) {
                            vsPopupTextArea.setText(string);
                        }
                    }
                );

                iAddOrRemoveBcm(bcmIndexVhf1, vhf1.isSelected());
                iAddOrRemoveBcm(bcmIndexVhf2, vhf2.isSelected());
                iSetGPIOPinState(bcmIndexAuxiliary1, auxCheckBox1.isSelected());
                iSetGPIOPinState(bcmIndexAuxiliary2, auxCheckBox2.isSelected());
                iSetGPIOPinState(bcmIndexAuxiliary3, auxCheckBox3.isSelected());
                iSetGPIOPinState(bcmIndexAuxiliary4, auxCheckBox4.isSelected());
            },
            0,
            Integer.parseInt( (String) props.get("poll_status_period_sec" ) ),
            TimeUnit.SECONDS
        );

        iSendPtt( false, restServerOS, restPortOS );
    }

    private String iGetOSRestServerFromConfig(
        final String restServerConfigKey)
    {
        final String configValue = (String) props.get(restServerConfigKey);

        final String effectiveValue;

        if ( configValue.isEmpty() ) {
            effectiveValue = SystemUtil.getAValidLocalIP( configValue, true );
            logger.info( "for this key: {}, we default to use local IP: {}", restServerConfigKey, effectiveValue );
        }
        else {
            effectiveValue = configValue;
        }

        return effectiveValue;
    }

    private void iSendShutdownToVS(String restServer, int restPort )
    {
        try {
            logger.info( "sending shutdown into {}/{}", restServer, restPort );

            final RestUtil.RestReturn lRestReturn = RestUtil.sendPut(
                "http",
                restServer,
                restPort,
                "/shutdown-app",
                1000,
                "");

            logger.info( "shutdown returns: {}", lRestReturn.getStatus() );
        }
        catch ( Exception ex )
        {
            logger.warn( ex.getMessage() );
        }
    }

    private void iSendPtt(boolean pttIsOn, String restServer, int restPort )
    {
        final String cmd = pttIsOn ? "/ptt-on" : "/ptt-off";

        logger.info( "sending {} into {}/{}", cmd, restServer, restPort );

        try {
            final RestUtil.RestReturn lRestReturn = RestUtil.sendPut(
                "http",
                restServer,
                restPort,
                cmd,
                1000,
                "");

            logger.info( "{} returns: {}", cmd, lRestReturn.getStatus() );
        }
        catch ( Exception ex )
        {
            logger.warn( ex.getMessage() );
        }
    }

    private void iAddStrut( GridPane gp, int row, int height )
    {
        final Rectangle rectangleA = new Rectangle(height, height);
        rectangleA.setFill(Color.TRANSPARENT);
        gp.add(rectangleA, 0, row);
    }

    /**
     *
     * @param addOrRemove   true: add, false: remove
     * @param bcmIndex
     */
    private void iAddOrRemoveBcm(int bcmIndex, boolean addOrRemove )
    {
        final String cmd = "/set-vhf-bcm-index";

        logger.info(
            "sending {} in {}/{} into {}:{}",
            cmd,
            bcmIndex, addOrRemove, restServerVS, restPortVS );

        try {


            final RestUtil.RestReturn lRestReturn = RestUtil.sendPut(
                "http",
                restServerVS,
                restPortVS,
                cmd,
                1000,
                String.format( "{ \"bcmIndex\": %d, \"action\": \"%s\" }", bcmIndex, addOrRemove ? "add" : "remove" ) );

            logger.info( "{} returns: {}", cmd, lRestReturn.getStatus() );
        }
        catch ( Exception ex )
        {
            logger.warn( ex.getMessage() );
        }
    }

    private void iSetGPIOPinState(int bcmIndex, boolean commandedState )
    {
        try {
            final String cmd = "/set-gpio-pin-state";

            logger.info( "sending {} in {}/{} into {}/{}", cmd, bcmIndex, commandedState, restServerVS, restPortVS );

            final RestUtil.RestReturn lRestReturn = RestUtil.sendPut(
                "http",
                restServerVS,
                restPortVS,
                cmd,
                1000,
                String.format( "{ \"bcmIndex\": %d, \"commandedState\": \"%s\" }", bcmIndex, commandedState ) );

            logger.info( "{} returns: {}", "/set-gpio-pin-state", cmd, lRestReturn.getStatus() );
        }
        catch ( Exception ex )
        {
            logger.warn( ex.getMessage() );
        }
    }

    private void iUpdateStatus(
        boolean includeRoundtripDelays,
        String restServer,
        int restPort,
        StatusIndicator statusIndicator,
        Label upTimeLabel,
        Consumer<String> statusConsumer )
    {
        try
        {
            final StringBuilder sb = new StringBuilder();

            {
                final RestUtil.RestReturn lRestReturn = RestUtil.sendGet(
                    "http",
                    restServer,
                    restPort,
                    "/get-latest-status",
                    1000);

                final String appVersion = new JSONObject(lRestReturn.getContent()).getString("appVersion");

                final boolean stationIsOperational = new JSONObject(lRestReturn.getContent()).getBoolean("stationOperational");
                statusIndicator.setState( stationIsOperational ? StatusIndicator.State.up : StatusIndicator.State.down );

                final long appStartTime = new JSONObject(lRestReturn.getContent()).getLong("appStartTime");
                final long currentTime = new JSONObject(lRestReturn.getContent()).getLong("currentTime");
                final String appUpTime = DurationFormatUtils.formatPeriod(
                    appStartTime,
                    currentTime,
                    "d'd' HH'h':mm'm'",
                    true,
                    TimeZone.getDefault() );
                Platform.runLater( () -> upTimeLabel.setText( appUpTime ) );

                sb.append( "Version:   ");
                sb.append( appVersion ).append("\n");

                sb.append( "IP/port:   ");
                sb.append( String.format( "%s:%d", restServer, restPort ) ).append( "\n" );

                sb.append( "Uptime:    ");
                sb.append( appUpTime ).append("\n");

                sb.append( "\n");
                sb.append( "Status:\n");
                sb.append( lRestReturn.getContent() ).append("\n\n");
            }

            {
                final RestUtil.RestReturn lRestReturn = RestUtil.sendGet(
                    "http",
                    restServer,
                    restPort,
                    "/get-latest-config",
                    1000);

                sb.append( "Config:\n");
                sb.append( lRestReturn.getContent() ).append("\n\n");
            }

            {
                final RestUtil.RestReturn lRestReturn = RestUtil.sendGet(
                    "http",
                    restServer,
                    restPort,
                    "/get-audio-drivers-info",
                    1000);

                sb.append( "Audio Drivers:\n");
                sb.append( lRestReturn.getContent() ).append("\n\n");
            }

            if ( includeRoundtripDelays )
            {
                final RestUtil.RestReturn lRestReturn = RestUtil.sendGet(
                    "http",
                    restServer,
                    restPort,
                    "/get-line-delay-stats",
                    1000);

                sb.append( "Round trip delay stats:\n");
                sb.append( lRestReturn.getContent() ).append("\n\n");
            }

            statusConsumer.accept( sb.toString() );
        }

        catch ( Exception ex )
        {
            logger.info( "{}:{} is unreachable", restServer, restPort );
            statusIndicator.setState(StatusIndicator.State.unknown);
            Platform.runLater( () -> upTimeLabel.setText( "n/a" ) );
        }
    }

    private void iCreatePopupWindow (
        Stage primaryStage,
        TextArea textArea,
        Consumer<WindowEvent> closeEventConsumer)
    {
        final Stage dialogStage = new Stage();
        dialogStage.setOnCloseRequest( (WindowEvent we) -> closeEventConsumer.accept(we) );
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        VBox dialogVbox = new VBox(0);
        dialogVbox.getChildren().add( textArea );
        Scene dialogScene = new Scene(dialogVbox);
        dialogStage.setScene(dialogScene);
        dialogStage.setResizable(false);
        dialogStage.show();
    }

    private void iLogHeader ( Logger logger ) throws Exception
    {
        logger.info( "app version:                 [" + SystemUtil.getGitVersion ( true ) + "]" );
        logger.info( "OS name:                     [" + System.getProperty ( "os.name" ) + "]" );
        logger.info( "OS version:                  [" + System.getProperty ( "os.version" ) + "]" );
        logger.info( "OS arch:                     [" + System.getProperty ( "os.arch" ) + "]" );
        logger.info( "host name:                   [" + InetAddress.getLocalHost ().getHostName () + "]");
        logger.info( "number of processors:        [" + Runtime.getRuntime().availableProcessors() + "]" );
        logger.info( "named command line args      [" + getParameters ().getNamed ( ) + "]");
        logger.info( "unnamed command line args    [" + getParameters ().getUnnamed ( ) + "]");
        logger.info( "extra java arguments:        [" + SystemUtil.getJavaExtraArguments () + "]");
        logger.info( "os type:                     [" + SystemUtil.getOSType () + "]");
        logger.info( "process id:                  [" + SystemUtil.getPIDForThisProcess("n/a") + "]");
        logger.info( "current directory:           [" + SystemUtil.getCurrentDirectory() + "]");
        logger.info( "log level:                   [" + LogConfigurator.singleton().getLogLevel() + "]");
        logger.info( "java args:                   [" + SystemUtil.getJavaExtraArguments ( ) + "]");
        logger.info( "java home:                   [" + SystemUtil.getEnvironmentOrSysProperty ( "java.home" ) + "]");
        logger.info( "java runtime version:        [" + SystemUtil.getEnvironmentOrSysProperty ( "java.runtime.version" ) + "]");
        logger.info( "java vm name:                [" + SystemUtil.getEnvironmentOrSysProperty ( "java.vm.name" ) + "]");
        logger.info( "java vm info:                [" + SystemUtil.getEnvironmentOrSysProperty ( "java.vm.info" ) + "]");
        logger.info( "java vm spec version:        [" + SystemUtil.getEnvironmentOrSysProperty ( "java.vm.specification.version" ) + "]");
        logger.info( "java vm spec vendor:         [" + SystemUtil.getEnvironmentOrSysProperty ( "java.vm.specification.vendor" ) + "]");
    }

    private static class KeystrokeHandler {

        private boolean keyOneIsPressed;
        private boolean keyTwoIsPressed;

        private KeystrokeHandler (Scene scene) {
            scene.setOnKeyPressed(
                (KeyEvent event) -> {
                    if ( KeyCode.S == event.getCode() ) {
                        keyOneIsPressed = true;
                    }
                    if ( KeyCode.V == event.getCode() ) {
                        keyTwoIsPressed = true;
                    }
                } );

            scene.setOnKeyReleased(
                (KeyEvent event) -> {
                    if ( KeyCode.S == event.getCode() ) {
                        keyOneIsPressed = false;
                    }
                    if ( KeyCode.V == event.getCode() ) {
                        keyTwoIsPressed = false;
                    }
                } );
        }

        private boolean isActivated() {
            return keyOneIsPressed && keyTwoIsPressed;
        }
    }
}

