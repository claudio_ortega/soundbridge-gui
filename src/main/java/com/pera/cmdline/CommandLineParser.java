/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 */

package com.pera.cmdline;

import com.pera.util.*;
import joptsimple.*;
import joptsimple.internal.*;
import java.io.*;
import java.util.*;

public class CommandLineParser
{
    private final OptionParser parser;

    public static CommandLineParser create()
    {
        return new CommandLineParser();
    }

    private CommandLineParser()
    {
        parser = new OptionParser();
    }

    public StartupConfigInfo getConfigFromArgs(
        String[] args,
        StartupConfigInfo defaultValues )
    {
        final OptionSpec<Void> help = parser
            .accepts( "help", "prints this help and exits" )
            .forHelp();

        final OptionSpec<Void> version = parser
            .accepts( "version", "prints the app version and exits" );

        final OptionSpec<String> configPath = parser
            .accepts( "config-file", "sets the path for the startup configuration file" )
            .withRequiredArg()
            .ofType( String.class )
            .defaultsTo( defaultValues.getConfigPath() );

        final OptionSpec<String> logLevel = parser
            .accepts( "log-level", "sets the log level, it should be any of these: {error, warn, info, debug, trace}" )
            .withRequiredArg()
            .ofType( String.class )
            .defaultsTo( defaultValues.getLogLevel() );

        final OptionSet options = parser.parse( args );

        final StartupConfigInfo startupConfig = StartupConfigInfo.create();

        startupConfig.setHelpRequested( options.has( help ) );
        startupConfig.setVersionRequested( options.has( version ) );
        startupConfig.setLogLevel( options.valueOf( logLevel ) );
        startupConfig.setConfigPath( options.valueOf( configPath ) );

        return startupConfig;
    }

    public void printHelp ( PrintStream ps ) throws IOException
    {
        ps.println();
        ps.println("RoiP - carries vhf radio sound and control across the internet." );
        ps.println();
        ps.println("git version: " + SystemUtil.getGitVersion( true ) );
        ps.println();
        ps.println("Usage: ");
        ps.println();
        parser.formatHelpWith( new HelpFormatter() );
        parser.printHelpOn( ps );
        ps.println();
        ps.println( "All command line options should start either with a single or double hyphen," );
        ps.println( "followed by the minimum number of letters that singles out an option." );
    }

    private static class HelpFormatter implements joptsimple.HelpFormatter
    {
        public String format( Map<String, ? extends OptionDescriptor> options )
        {
            final int[] COL_WIDTHS = {20,30,40};
            final StringBuilder sb = new StringBuilder();

            final String LINE_FORMAT =
                "%-"+ COL_WIDTHS[0] + "s" +
                "%-"+ COL_WIDTHS[1] + "s" +
                "%-"+ COL_WIDTHS[2] + "s" + "%n";

            sb.append( String.format( LINE_FORMAT, "option name", "default value", "description") );
            sb.append( Strings.repeat('-', COL_WIDTHS[0]+COL_WIDTHS[1]+COL_WIDTHS[2]) + "\n" );

            final List<String> orderedKeys = new LinkedList<>(options.keySet());
            orderedKeys.sort( Comparator.naturalOrder() );

            for ( final String nextKey : orderedKeys )
            {
                final OptionDescriptor descriptor = options.get( nextKey);

                if ( !descriptor.representsNonOptions() )
                {
                    final List<String> limitedLines = StringUtil.getLimitedLines( descriptor.description(), COL_WIDTHS[2] );

                    for ( int i=0; i< limitedLines.size(); i++ )
                    {
                        final String line = limitedLines.get(i);

                        if ( i == 0 )
                        {
                            sb.append(String.format(
                                LINE_FORMAT,
                                descriptor.options().get(0),
                                getDefaultExp(descriptor),
                                line)
                            );
                        }
                        else
                        {
                            sb.append(String.format(
                                LINE_FORMAT,
                                "",
                                "",
                                line)
                            );
                        }
                    }
                }

                sb.append("\n");
            }

            return sb.toString();
        }

        private static String getDefaultExp( OptionDescriptor descriptor )
        {
            final String ret;

            if ( ! descriptor.acceptsArguments() )
            {
                ret = "no arguments";
            }
            else
            {
                if ( descriptor.defaultValues().isEmpty() )
                {
                    ret = "no default value";
                }
                else
                {
                    ret = descriptor.defaultValues().toString();
                }
            }

            return ret;
        }
    }
}

