/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 */

package com.pera.cmdline;

public class StartupConfigInfo
{
    private boolean helpRequested;
    private boolean versionRequested;
    private String configPath;
    private String logLevel;

    public static StartupConfigInfo create()
    {
        return new StartupConfigInfo();
    }

    public static final StartupConfigInfo DEFAULT = create();

    private StartupConfigInfo()
    {
        helpRequested = false;
        versionRequested = false;
        configPath = "./config-gui.properties";
        logLevel = "warn";
    }

    public boolean isVersionRequested() {
        return versionRequested;
    }

    public StartupConfigInfo setVersionRequested(boolean value) {
        versionRequested = value;
        return this;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public StartupConfigInfo setLogLevel(String value) {
        logLevel = value;
        return this;
    }

    public String getConfigPath()
    {
        return configPath;
    }

    public StartupConfigInfo setConfigPath(String value)
    {
        configPath = value;
        return this;
    }

    public StartupConfigInfo setHelpRequested(boolean value)
    {
        helpRequested = value;
        return this;
    }

    public boolean getHelpRequested()
    {
        return helpRequested;
    }
}

