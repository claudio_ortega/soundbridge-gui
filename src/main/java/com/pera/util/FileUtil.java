
/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 */

package com.pera.util;

import com.google.common.base.Preconditions;
import org.apache.logging.log4j.*;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings( "ResultOfMethodCallIgnored" )
public class FileUtil
{
    private final static Logger logger = LogManager.getLogger();

    // can't instantiate this class
    private FileUtil() {}

    public static File getFileFromBaseAndAStringSequence(
            File aInBaseDir,
            String... aInStringList)
    {
        return getFileFromBaseAndAStringSequence( aInBaseDir, Arrays.asList(aInStringList) );
    }

    public static File getFileFromBaseAndAStringSequence(
            File aInBaseDir,
            List<String> aInStringList)
    {
        File lRet = aInBaseDir;

        for ( String lNext : aInStringList )
        {
            lRet = new File ( lRet, lNext );
        }

        return getFileWithAbsolutePath( lRet );
    }

    /**
     *
     * @param aInFailIfDirExists
     * @param dirs
     *
     * will createSingleton each dir in the variable argument list, provided it does not exist previous to the call
     * but if otherwise the requested dir does exist, then it will just ignore the request or fail depending on argument failIfDirExists
     */
    public static void createDirsUnderFileSystem(
            boolean aInFailIfDirExists,
            File... dirs)
    {
        for ( File lNextDirToCreate : dirs )
        {
            lNextDirToCreate = FileUtil.getFileWithAbsolutePath( lNextDirToCreate );

            if ( lNextDirToCreate.exists() )
            {
                if ( aInFailIfDirExists )
                {
                    throw new RuntimeException( "directory " + lNextDirToCreate + " already exists" );
                }
            }
            else
            {
                if ( ! lNextDirToCreate.mkdirs() )
                {
                    throw new RuntimeException( "failed to createSingleton directory " + lNextDirToCreate );
                }
            }
        }
    }

    /**
     *
     * @param aInFiles
     *      list of files to be tested for existence
     * @param aInExistenceConditionToMatch
     *      wheter we filter for existence or not existence
     * @return
     *      those files that hold the condition
     */
    public static List<File> getThoseWhichExistenceMatch(
            boolean aInExistenceConditionToMatch,
            List<File> aInFiles)
    {
        final List<File> lReturn = new LinkedList<>();

        for ( File next : aInFiles )
        {
            if ( next.exists() == aInExistenceConditionToMatch )
            {
                lReturn.add( next );
            }
        }

        return lReturn;
    }

    public static List<File> getListOfFilesFromListOfStrings( List<String> aInFileNames )
    {
        final List<File> lReturn = new LinkedList<>();

        for ( String nextName : aInFileNames )
        {
            lReturn.add( FileUtil.getFileWithAbsolutePath( new File( nextName ) ) );
        }

        return lReturn;
    }

    public static void replaceInFile(
            File aInFile,
            String aInRegexp,
            String aInReplacement )
            throws IOException
    {
        replaceInFile(
                aInFile,
                aInFile,
                aInRegexp,
                aInReplacement );
    }

    public static void replaceInFile(
            File aInFileFrom,
            File aInFileTo,
            String aInRegexp,
            String aInReplacement )
            throws IOException
    {
        List<String> lTemp = getLinesFromFile( aInFileFrom );

        for ( int i=0; i<lTemp.size() ; i++ )
        {
            String lOriginal = lTemp.get( i );
            String lReplaced = lOriginal.replaceAll( aInRegexp, aInReplacement );

            if ( ! lOriginal.equals( lReplaced ) )
            {
                lTemp.set( i, lReplaced );
            }

        }

        writeLinesIntoFile( lTemp, aInFileTo );
    }

    /**
     * The returned lines does not have the line separator at the end
     * @param file
     * @return
     * @throws IOException
     */
    public static List<String> getLinesFromFile( File file )
            throws IOException
    {
        return getLinesFromFileMatching( file, null, -1, true );
    }

    public static List<String> getLinesFromFileMatching(
            File file,
            String aInRegexpOrNUll )
            throws IOException
    {
        return getLinesFromFileMatching( file, aInRegexpOrNUll, -1, true );
    }

    public static List<String> getLinesFromFileMatching(
            File aInFile,
            String aInRegexpOrNUll,
            int aInMaxLinesToGet )
            throws IOException
    {
        return getLinesFromFileMatching ( aInFile, aInRegexpOrNUll, aInMaxLinesToGet, true );
    }

    /**
     * The returned lines does not have the line separator at the end
     * @param aInFile
     * @param aInRegexpOrNUll null matches all
     * @param aInMaxLinesToGet : -1 for no limit
     * @return
     * @throws IOException
     */
    public static List<String> getLinesFromFileMatching(
            File aInFile,
            String aInRegexpOrNUll,
            int aInMaxLinesToGet,
            boolean aInTransformSpecialChars )
            throws IOException
    {
        if ( aInTransformSpecialChars )
        {
            aInRegexpOrNUll = StringUtil.transformSpecialCharsInRegexp ( aInRegexpOrNUll );
        }

        final List<String> linesCollection = new LinkedList<>();

        final BufferedReader br = new BufferedReader( new InputStreamReader( new FileInputStream( aInFile ), StandardCharsets.US_ASCII) );

        String line;
        int linesAdded = 0;

        final Pattern lPattern = StringUtil.isNullOrEmpty( aInRegexpOrNUll ) ? null : Pattern.compile ( aInRegexpOrNUll );

        while ( ( line = br.readLine() ) != null )
        {
            if ( ( lPattern == null ) || lPattern.matcher( line ).matches() )
            {
                if ( linesAdded == aInMaxLinesToGet )
                {
                    break;
                }
                else
                {
                    linesCollection.add(line);
                    linesAdded++;
                }
            }
        }

        br.close();

        return linesCollection;
    }

    public static void copyLinesMatching(
            File aInInputFile,
            File aInOutputFile,
            IFilter<String> aInIFilter ) throws IOException
    {
        final BufferedReader br = new BufferedReader( new InputStreamReader( new FileInputStream( aInInputFile ), StandardCharsets.US_ASCII) );
        final PrintStream out = createPrintStreamFromFile( aInOutputFile );

        String line;

        while ( ( line = br.readLine() ) != null )
        {
            if ( aInIFilter.doesPass ( line ) )
            {
                out.println( line );
            }
        }

        br.close();

        out.flush();
        out.close();
    }

    /**
     *
     * @param aInFile
     * @throws IOException
     */

    /**
     *
     * @param  aInFile
     * @param  aInOneBasedLineNumber   : ==1 is for the first line in the file
     * @return The returned line ( with no line separator at the end ) at the linenumber or null if the file has less lines
     * @throws IOException
     */
    public static String getLineAtLineNumberFromFile( File aInFile, int aInOneBasedLineNumber )
    {
        try ( final BufferedReader br = new BufferedReader( new InputStreamReader( new FileInputStream( aInFile ), StandardCharsets.US_ASCII) ) )
        {
            String lRetLine = null;

            String line;
            int lCurrentLine = 0;

            while ( ( line = br.readLine() ) != null )
            {
                lCurrentLine++;

                if ( lCurrentLine == aInOneBasedLineNumber )
                {
                    lRetLine = line;
                    break;
                }
            }

            return lRetLine;
        }

        catch ( IOException ex )
        {
            throw new RuntimeException( ex );
        }
    }

    /**
     *
     * @param fileA
     * @param fileB
     * @param lineComparator
     * @param aInOutLoggingBuilder
     * @return false if the number of lines differ or otherwise if the lines on the same line number fails the passed comparator
     * @throws IOException
     */
    public static boolean compareFiles (
            File fileA,
            File fileB,
            Comparator<String> lineComparator,
            StringBuilder aInOutLoggingBuilder )
            throws IOException
    {
        List<String> linesInA = getLinesFromFile( fileA );
        List<String> linesInB = getLinesFromFile( fileB );

        boolean lRet;

        if ( linesInA.size() != linesInB.size() )
        {
            aInOutLoggingBuilder.append("file sizes differ").append( SystemUtil.Constants.LINE_SEPARATOR);
            aInOutLoggingBuilder.append("file ").append(fileA).append(" has:").append(linesInA.size()).append(" lines").append(
                    SystemUtil.Constants.LINE_SEPARATOR);
            aInOutLoggingBuilder.append("file ").append(fileB).append(" has:").append(linesInB.size()).append(" lines").append(
                    SystemUtil.Constants.LINE_SEPARATOR);

            lRet = false;
        }
        else
        {
            lRet = true;

            // linesInA == linesInB
            for ( int lineNumber=0; lineNumber<linesInA.size(); lineNumber++ )
            {
                String lineInA = linesInA.get( lineNumber );
                String lineInB = linesInB.get( lineNumber );

                if ( lineComparator.compare( lineInA, lineInB ) != 0 )
                {
                    aInOutLoggingBuilder.append(">>> fileA:[").append(fileA).append("], line number:[").append( lineNumber+1 ).append("], line:").append(lineInA).append(
                            SystemUtil.Constants.LINE_SEPARATOR);
                    aInOutLoggingBuilder.append("<<< fileB:[").append(fileB).append("], line number:[").append( lineNumber+1 ).append("], line:").append(lineInB).append(
                            SystemUtil.Constants.LINE_SEPARATOR);

                    lRet = false;

                    break;
                }
            }
        }

        return lRet;
    }

    // it the file passed in is relative, then it will refer to the current directory
    public static File getFileWithAbsolutePath(File aInFile)
    {
        try
        {
            return aInFile.getCanonicalFile();
        }
        catch ( IOException e )
        {
            throw new RuntimeException( e );
        }
    }

    /**
     *
     * @param aInFileName "./abc.txt" OR "abc.txt"
     * @return            "/home/cortega/data/sandboxes/git/master/java/mate/abc.txt"
     */
    public static File getFileWithAbsolutePath(String aInFileName)
    {
        return getFileWithAbsolutePath( new File( aInFileName ) );
    }

    /**
     * return constant two length strings from any byte
     * @param aInByte
     * @return
     */
    private static String getConstantLengthReadableStringFromByte( byte aInByte )
    {
        String lRet;

        switch ( aInByte )
        {
            case '\t':
                lRet = "\\t";
                break;

            case '\r':
                lRet = "\\r";
                break;

            case '\n':
                lRet = "\\n";
                break;

            default:
                byte[] lByteArray = new byte [2];

                if ( aInByte >= 32 && aInByte < 126 )
                {
                    // this is a printable character
                    lByteArray[0] = aInByte;
                    lByteArray[1] = ' ';
                    lRet = new String ( lByteArray, StandardCharsets.US_ASCII );
                }

                else
                {
                    // this is a control char, don't bother telling what is it,
                    // dumpStreamContentInHex() has already printed it out in hex
                    lRet = "\\c";
                }

                break;
        }

        return lRet;
    }

    public static void dumpStreamContentInHex (
            FileInputStream aInFileInputStream,
            PrintStream aInOutPrintStream ) throws IOException
    {
        int CHARS_PER_LINE = 10;

        StringBuilder lLineBuilderForHex = new StringBuilder();
        StringBuilder lLineBuilderForAscii = new StringBuilder();
        int lCharCount = 0;
        int lCharCountAtLastFlush = 0;

        int read;
        while ( ( read = aInFileInputStream.read() ) != -1 )
        {
            if ( ( lCharCount > 0 )  && ( lCharCount % CHARS_PER_LINE ) == 0 )
            {
                // flush:
                // print out intermediate result and reset content of both
                // StringBuilders

                aInOutPrintStream.println(
                        StringUtil.getRightPortionOfString(
                                "000000" + Integer.toString(lCharCount - CHARS_PER_LINE, 10),
                                6) + "-" +
                                StringUtil.getRightPortionOfString(
                                        "000000" + Integer.toString(lCharCount, 10), 6) +
                                "   [ " + lLineBuilderForHex + " ]" +
                                "   [ " + lLineBuilderForAscii + " ]");

                lLineBuilderForHex.setLength(0);
                lLineBuilderForAscii.setLength(0);

                lCharCountAtLastFlush = lCharCount;
            }

            lCharCount++;

            if ( lLineBuilderForHex.length() > 0 )
            {
                lLineBuilderForHex.append( " " );
            }

            lLineBuilderForHex.append( StringUtil.getRightPortionOfString ( "0" + Integer.toString( read, 16 ), 2 ) );

            if ( lLineBuilderForAscii.length() > 0 )
            {
                lLineBuilderForAscii.append( " " );
            }

            lLineBuilderForAscii.append(getConstantLengthReadableStringFromByte((byte) read));    // FileInputStream.read() doc states that it returns a "byte", so be it then
        }

        if ( lLineBuilderForHex.length() > 0 )
        {
            for ( int i = 0; i < CHARS_PER_LINE - ( lCharCount % CHARS_PER_LINE ); i++ )
            {
                lLineBuilderForHex.append( "   " );
                lLineBuilderForAscii.append( "   " );
            }

            aInOutPrintStream.println(
                    StringUtil.getRightPortionOfString(
                            "000000" + Integer.toString(lCharCountAtLastFlush, 10), 7) + "-" +
                            StringUtil.getRightPortionOfString(
                                    "000000" + Integer.toString(lCharCount, 10), 7) +
                            "   [ " + lLineBuilderForHex + " ]" +
                            "   [ " + lLineBuilderForAscii + " ]");
        }
    }

    public static void dumpFileContentInHex (
            File aInFile,
            File aInOutFile ) throws IOException
    {
        try (
            final FileInputStream in = new FileInputStream( FileUtil.getFileWithAbsolutePath( aInFile ) );
            final PrintStream out = createPrintStreamFromFile( aInOutFile ) )
        {
            dumpStreamContentInHex( in, out );
            in.close();
            out.flush();
        }
    }

    public static void writeLinesIntoFile(
            List<String> aInListOfLines,
            File aInOutFile )
            throws IOException
    {
        writeLinesIntoFile(
                aInListOfLines,
                aInOutFile,
                true,
                false );
    }

    public static void writeLinesIntoFile(
            List<String> aInListOfLines,
            File aInOutFile,
            boolean aInTerminateLastLineWithLineTerminator )
            throws IOException
    {
        writeLinesIntoFile (
                aInListOfLines,
                aInOutFile,
                aInTerminateLastLineWithLineTerminator,
                false );
    }


    /**
     *
     * @param aInListOfLines
     * @param aInOutFile     will continue one line per element in aInList
     * @throws IOException
     */
    public static void writeLinesIntoFile(
            List<String> aInListOfLines,
            File aInOutFile,
            boolean aInTerminateLastLineWithLineTerminator,
            boolean aInAppend )
            throws IOException
    {
        PrintStream out = createPrintStreamFromFile( aInOutFile, aInAppend );

        Iterator<String> lIt = aInListOfLines.iterator();

        while ( lIt.hasNext() )
        {
            String line = lIt.next();

            if( lIt.hasNext() )
            {
                out.println( line );
            }
            else
            {
                if ( aInTerminateLastLineWithLineTerminator )
                {
                    out.println( line );
                }
                else
                {
                    out.print( line );
                }
            }
        }

        out.flush();
        out.close();
    }

    public static void copyStream( InputStream in, OutputStream out )
            throws IOException
    {
        byte[] buffer = new byte[10*1024];
        int len;

        while ((len = in.read(buffer)) >= 0)
        {
            out.write(buffer, 0, len);
        }

        in.close();
        out.flush();
        out.close();
    }

    public static File copyFileToDir( File in, File outDir )
            throws IOException
    {
        Preconditions.checkArgument( outDir.isDirectory(), outDir + " should be a directory" );

        File lFileTo = new File( outDir, in.getName() );
        copyFile ( in, lFileTo );

        return lFileTo;
    }

    public static void copyFile( File in, File out )
            throws IOException
    {
        Preconditions.checkArgument( in.exists(), "the input file does not exist, input file is:" + in );
        Preconditions.checkArgument( in.canRead(), "the input file is not readable, input file is:" + in );

        FileUtil.createDirIfDoesNotExist ( out.getParentFile() );

        copyStream(
                new BufferedInputStream( new FileInputStream( in.getCanonicalPath() ) ),
                new BufferedOutputStream( new FileOutputStream( out.getCanonicalPath(), false ) ) );
    }

    public static List<File> unzip( File aInZipFile, File aInDirWhereToExtract ) throws IOException
    {
        final List<File> lRet = new LinkedList<>();

        ZipFile zipFile = new ZipFile( aInZipFile, ZipFile.OPEN_READ );

        Enumeration entries = zipFile.entries();

        while (entries.hasMoreElements())
        {
            ZipEntry entry = (ZipEntry) entries.nextElement();

            File lFile = new File( aInDirWhereToExtract, entry.getName() );
            File lParentFile = lFile.getParentFile();

            if (entry.isDirectory())
            {
                Preconditions.checkArgument( lFile.mkdirs() );
            }
            else
            {
                Preconditions.checkArgument( lParentFile.mkdirs() );
                copyStream( zipFile.getInputStream(entry), new BufferedOutputStream( new FileOutputStream( lFile ) ) );
                lRet.add( lFile );
            }
        }

        zipFile.close();

        return lRet;
    }

    public static List<ZipEntry> listZipFile ( File aInZipFile ) throws IOException
    {
        final List<ZipEntry> lReturn = new LinkedList<>();

        final ZipFile lZipFile = new ZipFile( aInZipFile, ZipFile.OPEN_READ );

        final Enumeration lEntries = lZipFile.entries();

        while ( lEntries.hasMoreElements() )
        {
            final ZipEntry lZipEntry = (ZipEntry) lEntries.nextElement();
            lReturn.add(lZipEntry);
        }

        lZipFile.close();

        return lReturn;
    }

    public static String getContentFromResourceFile ( Class aInClassForLoading, String aInResourceName, int aInMaxBytesToRead ) throws IOException
    {
        String lVersion = "n/a";

        try ( final InputStream lIS = aInClassForLoading.getResourceAsStream ( aInResourceName ) )
        {
            final byte[] buffer = new byte[aInMaxBytesToRead];   // how much we want to take out of this file?

            if ( lIS != null )
            {
                final int len = lIS.read ( buffer, 0, buffer.length );

                if ( len > 0 )
                {
                    lVersion = new String (
                        buffer,
                        0,
                        len,
                        Charset.forName ( "US-ASCII" ) );
                }
            }
        }

        return lVersion;
    }

    public interface IZipEntryNameFilter
    {
        String getEntryName ( File aInFile );
    }

    public static void zip(
            File aInZipContainerFile,
            List<File> aInFilesToZipInside,
            IZipEntryNameFilter aInZipNameFilter ) throws IOException
    {
        FileOutputStream dest = new FileOutputStream( aInZipContainerFile );
        ZipOutputStream out = new ZipOutputStream( new BufferedOutputStream( dest ) );

        final int BUFFER_LENGTH = 10*1024;
        byte data[] = new byte[BUFFER_LENGTH];

        for ( File lFile : aInFilesToZipInside )
        {
            FileInputStream fi = new FileInputStream( lFile );
            BufferedInputStream origin = new BufferedInputStream( fi, BUFFER_LENGTH );

            String lEntryName = aInZipNameFilter.getEntryName ( lFile );
            ZipEntry entry = new ZipEntry( lEntryName );
            out.putNextEntry(entry);

            int count;
            while( (count = origin.read(data, 0, BUFFER_LENGTH)) != -1)
            {
                out.write(data, 0, count);
            }

            origin.close();
            fi.close();
        }

        out.close();
    }

    public static void deletePlainFileIfExists(
            File aInFile,
            final boolean aInAssertOnFailure )
    {
        if ( !aInFile.isDirectory() && aInFile.exists() )
        {
            boolean aInDeletionWasOK = aInFile.delete();

            if ( ! aInDeletionWasOK )
            {
                final String lErrMsg = "deletion of file: [" + aInFile.getAbsolutePath() + "] failed" ;

                if ( aInAssertOnFailure )
                {
                    throw new IllegalStateException ( lErrMsg );
                }
                else
                {
                    System.err.println( lErrMsg );
                }
            }
        }
    }

    public static boolean isADescendantOfB( File aInA, File aInB )
    {
        boolean lRet = false;

        File lTmp = aInA.getParentFile();

        while ( lTmp != null )
        {
            if ( lTmp.equals( aInB ) )
            {
                lRet = true;
                break;
            }

            lTmp = lTmp.getParentFile();
        }

        return lRet;
    }

    public static void deletePlainFileIfExistsAndParentIfEmpty( File aInFile )
    {
        if ( !aInFile.isDirectory() && aInFile.exists() )
        {
            Preconditions.checkArgument( aInFile.delete(), "deletion of file: [" + aInFile + "] failed" );
            deleteDirIfEmpty( aInFile.getParentFile() );
        }
    }

    /**
     * NOTE:  if the did is not empty, will not try to delete it
     * @param aInDir
     */
    public static void deleteDirIfEmpty( File aInDir )
    {
        if ( aInDir.isDirectory() )
        {
            File[] lFilesInside = aInDir.listFiles();

            if ( lFilesInside != null && lFilesInside.length == 0 )
            {
                //noinspection ResultOfMethodCallIgnored
                Preconditions.checkArgument( aInDir.delete() );
                Preconditions.checkArgument( ! aInDir.exists(), "deletion of dir: [" + aInDir + "] failed" );
            }
        }
    }


    /**
     * NOTE:      will delete everything
     * @param     aInFile
     * @return    false if there is an error
     */
    public static boolean deleteFileRecursively ( File aInFile )
    {
        return deleteDirectoryRecursively( aInFile, true ) ;
    }

    /**
     * NOTE:      will delete everything
     * @param     aInRootFile
     * @return    false if there is an error
     */
    public static boolean deleteDirectoryRecursively(
            File aInRootFile,
            boolean aInIncludeRoot)
    {
        boolean lRet = true;

        Preconditions.checkNotNull ( aInRootFile );

        if ( aInRootFile.isDirectory() )
        {
            final File[] listFiles = aInRootFile.listFiles();

            if ( listFiles !=  null )
            {
                for ( File lNextDir : listFiles )
                {
                    lRet = deleteFileRecursively( lNextDir );
                    if ( ! lRet )
                    {
                        break;
                    }
                }
            }
        }

        if ( lRet && aInIncludeRoot )
        {
            lRet = aInRootFile.delete();
        }

        return lRet;
    }

    /**
     * do not use this method if the file exists and is a plain file,
     * the prerequisite for its use is the negation of this statement above
     *
     * @param aInDirectoryPath
     */
    @SuppressWarnings( "ResultOfMethodCallIgnored" )
    public static void createDirIfDoesNotExist( File aInDirectoryPath )
    {
        Preconditions.checkArgument( ! ( aInDirectoryPath.exists() && !aInDirectoryPath.isDirectory() ), "File: [" + aInDirectoryPath.getAbsolutePath() + "] already exists." );

        if ( ! aInDirectoryPath.exists() )
        {
            Preconditions.checkArgument( aInDirectoryPath.mkdirs() );
        }

        Preconditions.checkArgument( aInDirectoryPath.exists() && aInDirectoryPath.isDirectory(), "creation of dir: [" + aInDirectoryPath + "] failed" );
    }

    /**assertTrue
     * ie: a/b/c/d.tmp -> "tmp"
     *     if there is no "." in the name, then it returns the empty string
     * @param aInFile
     * @return
     */
    public static String getTermination ( File aInFile )
    {
        String lRet;

        List<String> lList = StringUtil.splitOverString( aInFile.getName(), "." );

        if ( lList.size() > 1 )
        {
            lRet = lList.get( lList.size() - 1 );
        }
        else
        {
            lRet = "";
        }

        return lRet;
    }

    /**
     * ie: a/b/c/d.tmp -> "d"
     * ie: a/b/c/d -> "d"
     * ie: a/b/c/d1.d2.d3.d4.tmp -> "d1.d2.d3.d4"
     * @param aInFile
     * @return
     */
    public static String getNameBeforeTermination ( File aInFile )
    {
        String lLastNameInPath = aInFile.getName();
        List<String> lAllDelimitedByDots = StringUtil.splitOverString( lLastNameInPath, "." );

        Preconditions.checkArgument( lAllDelimitedByDots.size() > 0 );

        String lResult;
        if ( lAllDelimitedByDots.size() > 1 )
        {
            // getAndShift all minus the last
            lResult = StringUtil.concatenate( false, false, ".", lAllDelimitedByDots.subList( 0, lAllDelimitedByDots.size() - 1 ) );
        }
        else
        {
            // getAndShift the only one
            lResult = lAllDelimitedByDots.get( 0 );
        }

        return lResult;
    }

    /**
     * does not append
     * @param aInOutFile
     * @return
     * @throws FileNotFoundException
     */
    public static PrintStream createPrintStreamFromFile ( File aInOutFile )
            throws UnsupportedEncodingException, FileNotFoundException
    {
        return createPrintStreamFromFile( FileUtil.getFileWithAbsolutePath( aInOutFile ), false );
    }

    public static PrintStream createNullPrintStream () throws UnsupportedEncodingException
    {
        return new PrintStream( createNullOutputStream(), false, "UTF-8");
    }

    private static OutputStream createNullOutputStream ()
    {
        return new OutputStream() {
            @Override
            public void write(int b) {
            }
        };
    }

    /**
     * @param aInOutFile
     * @return
     * @throws FileNotFoundException
     */
    public static PrintStream createPrintStreamFromFile ( File aInOutFile, boolean aInAppend )
            throws UnsupportedEncodingException, FileNotFoundException
    {
        return new PrintStream(
            new FileOutputStream( FileUtil.getFileWithAbsolutePath( aInOutFile ), aInAppend ),
            false,
            StandardCharsets.US_ASCII.name () );
    }

    private static final Comparator<File> REVERSE_MODIFICATION_TIME_COMPARATOR = ( o1, o2 ) -> {
        return (int) ( o2.lastModified() - o1.lastModified() );      // reverse order on time
    };

    public static List<File> getListOfFilteredFiles(
            File aInParentDirectory,
            IFilter<File> aInFileFilter )
    {
        final List<File> lList = CollectionsUtil.createList ();

        final File[] lAllFilesAsArrayOrNull = aInParentDirectory.listFiles ();

        if ( lAllFilesAsArrayOrNull != null )
        {
            for ( final File lNextSibling : lAllFilesAsArrayOrNull )
            {
                if ( aInFileFilter.doesPass( lNextSibling) )
                {
                    lList.add(lNextSibling);
                }
            }

            lList.sort ( REVERSE_MODIFICATION_TIME_COMPARATOR );
        }

        return lList;
    }

    /**
     *
     * @param aInFile
     * @return              will always return not null in case of not throwing exception
     * @throws Exception
     */
    public static byte[] getFileAsBytes ( File aInFile )
            throws IOException
    {
        return com.google.common.io.Files.toByteArray ( aInFile );
    }

    public interface IPathProducer
    {
        File getPathForIndex ( int index );
    }

    public static List<String> getMatchingLinesFromReader ( BufferedReader br, String aInRegexpOrNUll ) throws Exception
    {
        final Pattern lPatternOrNull =
                aInRegexpOrNUll == null
                        ? null
                        : Pattern.compile ( aInRegexpOrNUll );

        final List<String> linesCollection = new LinkedList<> ();

        String line;
        while (  ( line = br.readLine () ) != null )
        {
            if ( ( lPatternOrNull == null ) || lPatternOrNull.matcher ( line ).matches () )
            {
                linesCollection.add ( line );
            }
        }

        br.close ();

        return linesCollection;
    }

    public interface LineTransformer
    {
        List<String> transform( String aInLine, int aInLineNumber );
    }

    public interface OneLineTransformer
    {
        String transform( String aInLine, int aInLineNumber );
    }


    public static void copyTransformedFileContent (
            File aInFile,
            File aInOutFile,
            OneLineTransformer aInTransformer ) throws IOException
    {
        Preconditions.checkArgument( aInFile.exists(), "file " + aInFile + " should exist" );

        try(
            final BufferedReader lInput = new BufferedReader( new InputStreamReader( new FileInputStream( aInFile ), StandardCharsets.US_ASCII) );
            final PrintStream lOutput = createPrintStreamFromFile( aInOutFile ) )
        {
            String lLineContent;
            int lLineNumber = 0;

            while ( ( lLineContent = lInput.readLine () ) != null )
            {
                lLineNumber++;

                lOutput.println ( aInTransformer.transform ( lLineContent, lLineNumber ) );
            }
        }
    }

    public static void copyTransformedFileContent (
            File aInFile,
            File aInOutFile,
            LineTransformer aInLineContentTransformer ) throws IOException
    {
        Preconditions.checkArgument( aInFile.exists(), "file " + aInFile + " should exist" );

        try(
            final BufferedReader lInput = new BufferedReader( new InputStreamReader( new FileInputStream( aInFile ), StandardCharsets.US_ASCII) );
            final PrintStream lOutput = createPrintStreamFromFile( aInOutFile ) )
        {
            String lLineContent;
            int lLineNumber = 0;

            while ( ( lLineContent = lInput.readLine () ) != null )
            {
                lLineNumber++;

                for ( String lNext : aInLineContentTransformer.transform ( lLineContent, lLineNumber ) )
                {
                    lOutput.println ( lNext );
                }
            }
        }
    }

    public static File findFileRecursively ( File startingPoint, String aInName )
    {
        final List<File> dirContent = getDirContent(
            true,
            false,
            startingPoint,
            true,
            aInName,
            "",
            true
        );

        Preconditions.checkState (
            dirContent.size () > 0,
            "file not found, name: " + aInName + ", under directory: " + startingPoint
        );

        final File ret;

        if ( dirContent.size () == 1 )
        {
            ret = dirContent.get( 0 );
        }

        else
        {
            final List<File> copyList = CollectionsUtil.createList( dirContent );

            copyList.sort( ( o1, o2 ) -> o1.lastModified() - o2.lastModified() < 0 ? 1 : -1 );

            ret = copyList.get ( 0 );

            logger.warn (
                "more than one file with name: " + aInName +
                ", was found under directory: " + startingPoint +
                ", number found: " + copyList.size() +
                ", all files found: " + copyList +
                ", will be returning the newest file: " + ret
            );
        }

        return ret;
    }

        public static List<File> getDirContent(
        boolean aInIncludePlainFiles,
        boolean aInIncludeDirs,
        File inDir,
        boolean aInMatchOnlyName,
        String includeRegexpShell,
        String excludeRegexpShell,
        boolean recursive )
    {
        final List<File> lListOfFiles = new LinkedList<>();

        FileUtil.visitDirContent(
            aInIncludePlainFiles,
            aInIncludeDirs,
            inDir,
            aInMatchOnlyName,
            includeRegexpShell,
            excludeRegexpShell,
            recursive, aInFile -> lListOfFiles.add( aInFile ) );

        return Collections.unmodifiableList( lListOfFiles );
    }

    public interface FileVisitor
    {
        void doVisit(File aInFile);
    }


    /**
     * @param includeFiles       include files in the output list
     * @param includeDirs        include dirs in the output list
     * @param aInMatchOnlyName      if true matches only 'name.java' in '/tmp/x/y/xz/name.java'
     *                              if false matches on the whole path '/tmp/x/y/xz/name.java'
     * @param includeRegexpShell regexp in shell flavor like "*.*", "*.???", hello.txt", etc
     * @param excludeRegexpShell regexp in shell flavor like "*.*", "*.???", hello.txt", etc
     * @param recursive          handle subdirs as well
     * @param aInFileVisitor     visitor that will be invoked per file
     */
    public static void visitDirContent(
        boolean includeFiles,
        boolean includeDirs,
        File inDir,
        boolean aInMatchOnlyName,
        String includeRegexpShell,
        String excludeRegexpShell,
        boolean recursive,
        FileVisitor aInFileVisitor)
    {
        Preconditions.checkState ( inDir.exists(), inDir + " should exist" );
        Preconditions.checkState ( inDir.isDirectory(), inDir + " should be a directory" );

        final File[] filesInside = inDir.listFiles();

        if ( filesInside != null )
        {
            String includeRegexpPerl = StringUtil.convertRegexpFromShellToPerl(includeRegexpShell);
            String excludeRegexpPerl = StringUtil.convertRegexpFromShellToPerl(excludeRegexpShell);

            for ( final File sourceFile : filesInside )
            {
                final String whatToMatch;

                if (aInMatchOnlyName)
                {
                    whatToMatch = sourceFile.getName();
                }

                else
                {
                    whatToMatch = sourceFile.getAbsolutePath();
                }

                final boolean matchesIncludeFilter = !"".equals(includeRegexpPerl)
                        && whatToMatch.matches(includeRegexpPerl);
                final boolean matchesExcludeFilter = !"".equals(excludeRegexpPerl)
                        && whatToMatch.matches(excludeRegexpPerl);

                if ( matchesIncludeFilter && !matchesExcludeFilter )
                {
                    if ((includeFiles && sourceFile.isFile())
                            || (includeDirs && sourceFile.isDirectory()))
                    {
                        aInFileVisitor.doVisit(sourceFile);
                    }
                }

                if (recursive && sourceFile.isDirectory())
                {
                    visitDirContent(
                        includeFiles,
                        includeDirs,
                        sourceFile,
                        aInMatchOnlyName,
                        includeRegexpShell,
                        excludeRegexpShell,
                        recursive,
                        aInFileVisitor);
                }
            }
        }
    }

    public static Map<String,String> loadFromPropertiesFile( File f ) throws IOException {
        try ( final InputStream inStream = new FileInputStream(f) )
        {
            final Properties props = new Properties();
            props.load(inStream);
            return (Map<String,String>) new HashMap(  props );
        }
    }
}

