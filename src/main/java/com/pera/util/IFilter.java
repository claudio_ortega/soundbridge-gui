

/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 */

package com.pera.util;

public interface IFilter<T>
{
    boolean doesPass(T aInTUnderTest);
}