/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 */

package com.pera.suites;

import com.pera.util.TestLog;
import com.pera.util.TestSystemUtil;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith (Suite.class)
@Suite.SuiteClasses(
    {
        TestSystemUtil.class,
        TestLog.class,         // keep at last, as it changes the log level
    }
)

public class TestRegressionSuite
{
}
