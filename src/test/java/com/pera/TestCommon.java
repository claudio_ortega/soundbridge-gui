/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 */

package com.pera;

import com.pera.util.LogConfigurator;
import org.apache.logging.log4j.*;
import org.junit.*;

public abstract class TestCommon
{
    // override as needed
    protected Level getLoggingLevel()
    {
        return Level.INFO;
    }

    @BeforeClass
    public static void beforeClass()
    {
        // IMPORTANT !!!! this has to happen BEFORE ANY call to LogManager.getLogger();
        LogConfigurator.singleton().init();
    }

    @Before
    public void beforeInstance()
    {
        LogConfigurator.singleton().setLevel( getLoggingLevel ().toString () );
    }
}



