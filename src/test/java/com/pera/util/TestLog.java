/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 */

package com.pera.util;

import com.pera.*;
import org.apache.logging.log4j.*;
import org.junit.*;

public class TestLog extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Override
    protected Level getLoggingLevel()
    {
        return Level.DEBUG;
    }

    @Test
    public void test00 ()
    {
        // no call to LogConfigurator.singleton().init(), as it is done from the @BeforeClass method in TestCommon
        // no call to LogConfigurator.singleton().setLevel(), as it is done from the @Before method in TestCommon

        logAtAllLevels ();
    }

    private void logAtAllLevels ()
    {
        logger.trace("hello world! (1)");
        logger.debug("hello world! (2)");
        logger.info("hello world! (3)");
        logger.warn("hello world! (4)");
        logger.error("hello world! (5)");
        logger.error("");
    }
}