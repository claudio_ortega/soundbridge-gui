/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 */

package com.pera.util;

import com.google.common.base.*;
import com.pera.*;
import com.pera.mainfx.*;
import org.junit.*;
import org.apache.logging.log4j.*;

public class TestMain extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    protected Level getLoggingLevel()
    {
        return Level.DEBUG;
    }

    @Test
    public void testVersion()
    {
        final int status = MainApp.mainAlt( "--version" );
        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == 0 );
    }

    @Test
    public void testHelp()
    {
        final int status = MainApp.mainAlt( "--help" );
        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == 0 );
    }

    @Test
    public void testNormal ()
    {
        final int status = MainApp.mainAlt( "--config-file=./src/bin/config-gui.properties" );
        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == 0 );
    }
}