#!/usr/bin/env bash

cd $(dirname ${0})
java -Xmx300m -Xms300m -cp *.jar com.pera.mainfx.MainApp ${*}
cd -
